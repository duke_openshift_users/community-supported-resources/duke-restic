** Work In Progress **

This is a collection of resources to build a restic image to use for backing up one PVC to another within your project.

This configuration is offered as a [DOUG Community Supported Resource](https://duke_openshift_users.pages.oit.duke.edu/community-supported-resources/guide/)

An example restic-backup-client deployment is included in [k8s/restic-deploymentconfig.yaml](k8s/restic-deploymentconfig.yaml) which has comments where you need to make adjustments for your projects configuration.

An example restic-backup-runner cronjob is included in [k8s/restic-cronjob.yaml](k8s/restic-cronjob.yaml) which similarly to the deploymentconfig has notes for where to adjust to your needs.

The basic architecture is that there is a source PVC configured at the /data path in a restic container and a destination PVC configured on the /backup path. The entrypoint for the restic-backup-runner pod will create a restic repo on /backup if necessary and perform a restic backup from /data into /backup every time it is run. The restic-backup-client is intended to be used as a point of interaction between the user and the backup repository for either ad-hoc restores, backups or integrity checks.

Contributions welcome according see: [CONTRIBUTING.md](CONTRIBUTING.md)
