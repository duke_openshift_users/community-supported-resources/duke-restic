#!/bin/bash

set -e


if [[ ! -d ${RESTIC_REPOSITORY}/data ]]; then
  /usr/bin/restic init -r ${RESTIC_REPOSITORY} && \
  /usr/bin/restic backup --host=${OPENSHIFT_BUILD_NAMESPACE} --parent "" ${RESTIC_SOURCE}
else
  /usr/bin/restic backup --host=${OPENSHIFT_BUILD_NAMESPACE} --parent "" ${RESTIC_SOURCE}
fi
